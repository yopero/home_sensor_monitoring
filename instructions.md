- create user
    ./mainflux-cli users token user@example.com contrasena
- get user token
    ./mainflux-cli users token user@example.com contrasena
- create thing
    ./mainflux-cli things create '{"type":"device", "name":"esp8266_sensors_001"}' $USERTOKEN
- get thing id and token
    ./mainflux-cli things get a146689b-faba-4ff6-8340-e90ce6d683f4 $USERTOKEN
{
  "id": "a146689b-faba-4ff6-8340-e90ce6d683f4",
  "key": "040d2f88-9f7f-4ae2-b997-e28c1246f209",
  "name": "esp8266_sensors_001"
}
- create channels
    ./mainflux-cli channels create '{"name":"pm_temp_hum_sensors"}' $USERTOKEN
- get channel details
    ./mainflux-cli channels get 146dc4e1-ffe6-4eac-8872-4987633eeb2a $USERTOKEN
{
  "id": "146dc4e1-ffe6-4eac-8872-4987633eeb2a",
  "name": "pm_temp_hum_sensors"
}

- connect thing to channel
    ./mainflux-cli things connect a146689b-faba-4ff6-8340-e90ce6d683f4 146dc4e1-ffe6-4eac-8872-4987633eeb2a $USERTOKEN
